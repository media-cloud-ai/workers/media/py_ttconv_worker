"""
MCAI ttconv worker
"""
import logging
import time
import os

import sys
import xml.etree.ElementTree as et
from pathlib import Path

import ttconv.imsc.reader as imsc_reader
import ttconv.imsc.writer as imsc_writer
import ttconv.scc.reader as scc_reader
import ttconv.srt.reader as srt_reader
import ttconv.srt.writer as srt_writer
import ttconv.stl.reader as stl_reader
import ttconv.vtt.writer as vtt_writer
from ttconv.imsc.config import IMSCWriterConfiguration
from ttconv.isd import ISDConfiguration
from ttconv.scc.config import SccReaderConfiguration
from ttconv.stl.config import STLReaderConfiguration
from ttconv.tt import (
    FileTypes,
    progress_callback_read,
    read_config_from_json,
    progress_callback_write,
)


import mcai_worker_sdk as mcai


class TtConvParameters(mcai.WorkerParameters):
    source_path: str
    destination_path: str


class TtConvWorker(mcai.Worker):
    def process(
        self,
        handle_callback: mcai.McaiChannel,
        parameters: TtConvParameters,
        job_id: int,
    ):
        print("Job ID: ", job_id)
        print("Parameters: ", parameters)

        LOGGER = logging.getLogger("ttconv")
        inputfile = parameters.source_path
        outputfile = parameters.destination_path

        print(inputfile)
        print(outputfile)

        json_config_data = None
        _input_filename, input_file_extension = os.path.splitext(inputfile)
        _output_filename, output_file_extension = os.path.splitext(outputfile)

        reader_type = FileTypes.get_file_type(None, input_file_extension)
        writer_type = FileTypes.get_file_type(None, output_file_extension)

        print(reader_type)
        print(writer_type)

        if reader_type is FileTypes.TTML:
            # Parse the xml input file into an ElementTree
            tree = et.parse(inputfile)
            # Pass the parsed xml to the reader
            model = imsc_reader.to_model(tree, progress_callback_read)

        elif reader_type is FileTypes.SCC:
            file_as_str = Path(inputfile).read_text()
            # Read the config
            reader_config = read_config_from_json(
                SccReaderConfiguration, json_config_data
            )
            # Pass the parsed xml to the reader
            model = scc_reader.to_model(
                file_as_str, reader_config, progress_callback_read
            )

        elif reader_type is FileTypes.STL:
            # Read the config
            reader_config = read_config_from_json(
                STLReaderConfiguration, json_config_data
            )
            # Open the file and pass it to the reader
            #
            with open(inputfile, "rb") as f:
                model = stl_reader.to_model(f, reader_config, progress_callback_read)

        elif reader_type is FileTypes.SRT:
            # Open the file and pass it to the reader
            with open(inputfile, "r", encoding="utf-8") as f:
                model = srt_reader.to_model(f, None, progress_callback_read)

        else:
            exit_str = f"Input file is {inputfile} is not supported"
            LOGGER.error(exit_str)
            sys.exit(exit_str)

        if writer_type is FileTypes.TTML:
            # Read the config
            writer_config = read_config_from_json(
                IMSCWriterConfiguration, json_config_data
            )
            # Construct and configure the writer
            tree_from_model = imsc_writer.from_model(
                model, writer_config, progress_callback_write
            )
            # Write out the converted file
            tree_from_model.write(outputfile, encoding="utf-8")

        elif writer_type is FileTypes.SRT:
            # Read the config
            writer_config = read_config_from_json(ISDConfiguration, json_config_data)
            # Construct and configure the writer
            srt_document = srt_writer.from_model(
                model, writer_config, progress_callback_write
            )
            # Write out the converted file
            with open(outputfile, "w", encoding="utf-8") as srt_file:
                srt_file.write(srt_document)

        elif writer_type is FileTypes.VTT:
            # Construct and configure the writer
            vtt_document = vtt_writer.from_model(model, None, progress_callback_write)
            # Write out the converted file
            with open(outputfile, "w", encoding="utf-8") as vtt_file:
                vtt_file.write(vtt_document)

        else:
            exit_str = f"Output file is {outputfile} is not supported"
            LOGGER.error(exit_str)
            sys.exit(exit_str)

        return {}


def main():
    description = mcai.WorkerDescription(__package__)
    worker = TtConvWorker(TtConvParameters, description)
    worker.start()


if __name__ == "__main__":
    main()
