FROM python:3.9

WORKDIR /src

ADD py_ttconv_worker /src/py_ttconv_worker

COPY pyproject.toml /src

RUN pip install .

ENV AMQP_QUEUE=job_ttconv_worker

CMD python3 py_ttconv_worker/worker.py
